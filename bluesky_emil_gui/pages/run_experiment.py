from qtpy.QtWidgets import (
    QWidget,
    QHBoxLayout,
    QVBoxLayout,
    QGridLayout,
    QFrame,
    QSizePolicy,
    QRadioButton,
    QPushButton,
    QGroupBox,
)

from qtpy.QtCore import Qt, Signal, Slot, QTimer
from qtpy.QtGui import QFont, QPalette

from bluesky_widgets.models.plot_builders import Lines

from bluesky_widgets.models.plot_specs import Figure, Axes

from bluesky_widgets.qt.search import QtSearch
from bluesky_widgets.qt.run_engine_client import (
    QtReEnvironmentControls,
    QtReManagerConnection,
    QtReQueueControls,
    QtReExecutionControls,
    QtReStatusMonitor,
    QtRePlanQueue,
    QtRePlanHistory,
    QtReRunningPlan
)

from bessyii_bluesky_widgets.qt.QtQueueControl import QtQueueControl
from bessyii_bluesky_widgets.qt.QtHistoryControl import QtHistoryControl
from bessyii_bluesky_widgets.qt.QtRunningPlan import QtReRunningPlan

from bessyii_bluesky_widgets.qt.input import (
    QtPlanButton,
    QtAuthButton,
)

from bessyii_bluesky_widgets.qt.plan_creator import (
    QtRePlanEditor,
)


from bessyii_bluesky_widgets.qt.pvstatus import (
    QtPVStatusViewer2Columns,
)

from bessyii_bluesky_widgets.qt.new_plot_widget import (
    QtFigures,
)


class QtRunExperiment(QWidget):
    def __init__(self, model, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.model = model

        vbox_00 = QVBoxLayout()

        hbox_10 = QHBoxLayout()
        hbox_10.addStretch()
        hbox_10.addWidget(QtAuthButton(parent=self, model=self.model.icat, sys_name='ICAT'), stretch=0)
        hbox_10.setContentsMargins(0,0,0,0)
        vbox_00.addLayout(hbox_10)

        vbox_00.addWidget(QtReRunningPlan(self.model))

        hbox_12 = QHBoxLayout()
        re_plan_editor = QtRePlanEditor(self.model)
        re_plan_editor.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Expanding)
        hbox_12.addWidget(re_plan_editor, stretch=1)
        
        vbox_new = QVBoxLayout()
        vbox_new.addWidget(QtQueueControl(self.model.run_engine))
        vbox_new.addWidget(QtHistoryControl(self.model.run_engine))
        hbox_12.addLayout(vbox_new)
        vbox_00.addLayout(hbox_12)    

        self.setLayout(vbox_00)