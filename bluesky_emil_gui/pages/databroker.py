from qtpy.QtWidgets import (
    QWidget,
    QHBoxLayout,
    QVBoxLayout,
)

from bessyii_bluesky_widgets.qt.search import (
    QtSearchWithButton,
)

from bessyii_bluesky_widgets.qt.figures import QtAddCustomPlot
from bluesky_widgets.qt.figures import (
    QtFigures
)

class QtSearchAndView(QWidget):
    def __init__(self, model, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.model = model
        layout = QHBoxLayout()
        self.setLayout(layout)
        layout.addWidget(QtSearchWithButton(model.search))
        plot_layout = QVBoxLayout()
        plot_layout.addWidget(QtAddCustomPlot(self.model)) # this is used to select x and y from dataset
        # How would this work with a list of auto plot builders?
        for auto_plot_builder in self.model.auto_plot_builders:
           plot_layout.addWidget(QtFigures(auto_plot_builder.figures))

        #plot_layout.addWidget(QtFigures(self.model.auto_plot_builders[0].figures)) 
        layout.addLayout(plot_layout)