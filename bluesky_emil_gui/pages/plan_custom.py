import ast
import re

from bluesky_widgets.qt.run_engine_client import _QtRePlanEditorTable, _QtReViewer, _QtReEditor

try:
    # cytools is a drop-in replacement for toolz, implemented in Cython
    from cytools import partition
except ImportError:
    from toolz import partition

from qtpy.QtWidgets import (
    QWidget,
    QPushButton,
    QVBoxLayout,
    QHBoxLayout,
    QTableWidget,
    QTableWidgetItem,
    QHeaderView,
    QTabWidget,
    QComboBox,
    QCheckBox,
    QLineEdit,
    QStyledItemDelegate,
)
from qtpy.QtCore import Signal, Slot


class ReadOnlyDelegate(QStyledItemDelegate):
    """
    The content applied with this class can not be changed.
    """
    def createEditor(self, parent, option, index):
        return

def getDefaultPlans():
    """
    These are the default plans and parameters, which will be shown in the custom plan widget. If the default value is
    " ", then the input must be filled in the table; if the default value is "None", then the input can still be "None".
    """
    data_list = [
        {"scan": {"detectors": " ", "mot": " ", "start": " ", "stop": " ", "num": 1, "per_step": "None", "md": "None"}},
        {"count": {"detectors": " ", "num": 1, "delay": "None", "per_shot": "None", "md": "None"}},
        {"flyscan": {"detectors": " ", "flyer": " ", "start": " ", "stop": " ", "vel": 0.2, "delay": 0.2, "md": "None"}},
        {"xas": {"End_station": [" ", "OAESE", "SISSY1"],
                 "Mono": ["u17_dcm.en", "u17_pgm.en", "ue48_pgm.en"], "Mode": [" ", "step", "cont"],
                 "Velocity(eV/s)": [" ", "0.5", "1"], "Start_Energy": " ", "Stop_Energy": " ", "Step_Size(eV)": " ",
                 "Current_Range": [" ", "[100-200]", "[200-300]"],
                 "Use_shutter": ["False", "True"],
                 "md": "None"}},
        {"mv": {"mot": " ", "set_point": " "}},
        {"mvr": {"mot": " ", "set_point": " "}}]

    _default_parameters_dict = {}
    for i in data_list:
        _default_parameters_dict.update(i)
    return _default_parameters_dict


class EmilSpecialPlansWidget(QTableWidget):
    signal_update_widgets = Signal()
    signal_switch_tab = Signal(str)
    signal_allowed_plan_changed = Signal()

    def __init__(self, model, parent=None):
        super().__init__(parent)
        self.model = model

        self._wd_editor = _QtRePlanEditorTable(self.model, editable=False, detailed=True)

        self.totalLayout = QVBoxLayout(self)
        self.chooseLayout = QHBoxLayout()

        self.combo = QComboBox(self)
        self.plan_dict = self._get_default_parameters()
        self.combo_list = [" "] + list(self.plan_dict.keys())
        self.combo.addItems(self.combo_list)
        self.combo.currentTextChanged.connect(self.on_clicked)
        self.chooseLayout.addWidget(self.combo)

        self.lineEdit = QLineEdit()
        self.current_text = None
        self.chooseLayout.addWidget(self.lineEdit)

        self.h_box = QHBoxLayout()
        self._tableWidget = QTableWidget()

        self.h_box.addWidget(self._tableWidget)
        self.totalLayout.addLayout(self.chooseLayout)
        self.totalLayout.addLayout(self.h_box)

        self._pb_add_to_queue = QPushButton("Add to Queue")
        self._pb_cancel = QPushButton("Cancel")
        self._pb_add_to_queue.setEnabled(False)
        self._pb_cancel.setEnabled(False)
        self._pb_add_to_queue.clicked.connect(self._pb_add_to_queue_clicked)
        self._pb_cancel.clicked.connect(self._pb_cancel_clicked)

        self.addMotor_button = QPushButton(self._tableWidget)
        self.addMotor_button.setText("Add motors")
        self.addMotor_button.clicked.connect(self.klick_addMotors)
        self.addMotor_button.setEnabled(False)

        self.hbox = QHBoxLayout()
        self.hbox.addStretch(1)
        self.hbox.addWidget(self._pb_add_to_queue)
        self.hbox.addWidget(self._pb_cancel)
        self.hbox.addWidget(self.addMotor_button)
        self.totalLayout.addLayout(self.hbox)

        self.insert_motornumber = None
        self.param_list = None
        self.param_dict = None

    def  _get_default_parameters(self):
        """
        Get the default plans and parameters for the custom plan widget
        """
        return getDefaultPlans()

    def _clear_widgets(self):
        """
        Clear the widgets
        """
        for row in reversed(range(self._tableWidget.rowCount())):
            self._tableWidget.removeRow(row)

    def _pb_add_to_queue_clicked(self):
        """
        Add item to queue
        """
        if self.current_text != " ":
            try:
                item = self.setParamItem()
                if item is None:
                    self.lineEdit.setText("Input values are not valid")
                else:
                    self.model.queue_item_add(item=item)
                    self._wd_editor.show_item(item=None)
                    self.signal_switch_tab.emit("view")
                    self._edit_mode_enabled = False
                    self._current_item_source = ""
                    reEditor = _QtReEditor(self.model)
                    reEditor._show_item_preview()
                    self.lineEdit.setText(" ")
            except Exception as ex:
                print("Exception in : ", ex)

        else:
            self.lineEdit.setText("please choose a plan")

    def _pb_cancel_clicked(self):
        """
        Cancel the plan.
        """
        self.combo.setCurrentText(" ")
        self._pb_add_to_queue.setEnabled(False)
        self._pb_cancel.setEnabled(False)
        self._wd_editor.show_item(item=None)
        self._edit_mode_enabled = False
        self._queue_item_type = ""
        self._queue_item_name = ""
        self._current_item_source = ""
        reEditor = _QtReEditor(self.model)
        reEditor._show_item_preview()
        self._clear_widgets()
        self.lineEdit.setText(" ")
        reEditor = _QtReEditor(self.model)
        reEditor._show_item_preview()
        self.lineEdit.setText("please choose a plan")

    def on_combobox_func(self, text):
        """
        Get the chosen plan name as current_text
        """
        self.current_text = text

    def on_clicked(self, text):
        """
        Create table of the chosen plan with clicking the button. Only "scan", "mv", "mvr" have the add-motor function.
        """
        self.current_text = text
        try:
            if text != " ":
                self.lineEdit.setText("Set {} parameters...".format(text))
                self.createTable()
                self.addMotor_button.setEnabled(False)
                if self.current_text in ["scan", "mv", "mvr"]:
                    self.addMotor_button.setEnabled(True)
            else:
                self.lineEdit.setText("please choose a plan...")
                self._clear_widgets()
                self._pb_add_to_queue.setEnabled(False)
                self._pb_cancel.setEnabled(False)
                self.addMotor_button.setEnabled(False)
        except Exception as ex:
            print("Exception in on_clicked(): ", ex)

    def checkInput(self, dict_item):
        """
        Check the input. The default value" " area must be filled in. The default "None" area can still be "None".
        """
        if dict_item is not None:
            value_list = []
            for key in dict_item.keys():
                value = dict_item.get(key)
                if value == " ":
                    return None
                else:
                    value_list.append(value)
            if all(v in ["None", " ", "NONE"] for v in value_list):
                return None
            else:
                return dict_item
        else:
            return None

    def setParamItem(self):
        """
        Set the dict item of the plans. The item consists of the default parameters and the inputs.
        """
        command = self.current_text
        item = {}

        if command != "xas":
            dict_item = self.checkInput(self.getValueList())
            if dict_item is not None:
                if command == "mv" or command == 'mvr':
                    motor = dict_item["mot"]
                    set_point = float(dict_item["set_point"])
                    list_args = (motor, set_point)
                    # Note that we have no md dict since mv is a stub plan
                    if self.insert_motornumber != 0:
                        for i in range(self.insert_motornumber):
                            new_motor_args = (
                            dict_item["new mot " + str(i)], float(dict_item["new set_point " + str(i)]))
                            list_args += new_motor_args

                    item = {'item_type': 'plan',
                            'name': command,
                            'args': list_args}
                    return item

                elif command == "scan":
                    det_devices = dict_item["detectors"]
                    det_list = re.findall(r'[^,;\s]+', det_devices)
                    mot_name = dict_item["mot"]
                    start = dict_item["start"]
                    stop = dict_item["stop"]
                    number = dict_item["num"]
                    per_step = dict_item["per_step"]
                    md_str = dict_item["md"]
                    mot_list = []
                    motors = [mot_name]
                    list_args = []
                    if start != " " and "stop" != " ":
                        try:
                            start = float(start)
                            stop = float(stop)
                            list_args = [det_list, mot_name, start, stop]
                        except Exception as ex:
                            print("Exception in the list_args in scan :", ex)
                    if number in ["None", " ", "NONE"]:
                        self.lineEdit.setText("The number of points must be provided")
                        raise ValueError("The number of points must be provided")
                    if not (float(number).is_integer() and int(number) > 0):
                        self.lineEdit.setText("The parameter `num` is expected to be a number")
                        raise ValueError(f"The parameter `num` is expected to be a number of "
                                         f"steps (not step size!) It must therefore be a "
                                         f"whole number. The given value was {number}.")
                    if per_step in ["None", " ", "NONE"]:
                        per_step = None
                    else:
                        per_step = [int(per_step)]
                    number = int(number)
                    if self.insert_motornumber != 0:
                        for i in range(self.insert_motornumber):
                            motors.append(dict_item["new mot " + str(i)])
                            mot_list.append("mot" + str(i))
                            mot_list.append("start" + str(i))
                            mot_list.append("stop" + str(i))
                            list_args.append(dict_item["new mot " + str(i)])
                            list_args.append(float(dict_item["new start " + str(i)]))
                            list_args.append(float(dict_item["new stop " + str(i)]))
                    md = {}
                    if md_str is not None:
                        if md_str in ["None", " ", "NONE"]:
                            pass
                        else:
                            md_dict = ast.literal_eval(md_str) # convert string into dict
                            md.update(md_dict)
                    else:
                        print("it is None")
                    if per_step in ["None", " ", "NONE"]:
                        item = {'item_type': 'plan',
                                'name': 'scan',
                                'args': list_args,
                                'kwargs': {'num': number, 'md':md}
                                }
                    elif per_step not in ["None", " ", "NONE"]:
                        item = {'item_type': 'plan',
                                'name': 'scan',
                                'args': list_args,
                                'kwargs': {'num': number, 'per_step': per_step,'md':md}
                                }
                    return item
                elif command == "count":
                    det_devices = dict_item["detectors"]
                    det_list = re.findall(r'[^,;\s]+', det_devices)
                    number = dict_item["num"]
                    delay = dict_item['delay']
                    md_str = dict_item["md"]
                    per_shot = dict_item['per_shot']
                    if not number:
                        raise ValueError("The count number can not be None")
                    if number in ["None", " ", "NONE"]:
                        raise ValueError("The count number can not be None")

                    if not (float(number).is_integer() and int(number) > 0):
                        print("The count number {} is not a valid count number".format(int(number)))
                        raise ValueError(f"The parameter `num` is expected to be the "
                                         f"count number, It must therefore be a "
                                         f"whole number. The given value was {number}.")
                    number = int(number)
                    if delay in ["None", " ", "NONE"]:
                        delay = None
                    else:
                        delay = float(delay)
                    if per_shot in ["None", " ", "NONE"]:
                        per_shot = None
                    else:
                        ## per_shot usage is not clear, so just use None ##TODO
                        print("check the per_shot usage in the plan count please. ".format(per_shot))
                        pass
                    md = {}
                    if md_str in ["None", " ", "NONE"]:
                        pass
                    else:
                        md_dict = ast.literal_eval(md_str)  # convert string into dict
                        md.update(md_dict)

                    if delay in ["None", " ", "NONE"]:
                        item = {'item_type': 'plan',
                                'name': 'count',
                                'args': [],
                                'kwargs': {"detectors": det_list, 'num': int(number),'md':md}
                                }

                    elif per_shot not in ["None", " ", "NONE"]:
                        item = {'item_type': 'plan',
                                'name': 'count',
                                'args': [],
                                'kwargs': {"detectors": det_list, 'num': int(number), 'per_shot': per_shot,'md':md}
                                }
                    elif delay not in ["None", " ", "NONE"]:
                        item = {'item_type': 'plan',
                                'name': 'count',
                                'args': [],
                                'kwargs': {"detectors": det_list, 'num': int(number), 'delay': float(delay),'md':md}
                                }
                    else:
                        item = {'item_type': 'plan',
                                'name': 'count',
                                'args': [],
                                'kwargs': {"detectors": det_list, 'num': int(number), 'delay': float(delay),
                                           'per_shot': per_shot,'md':md}
                                }
                    return item

                elif command == "flyscan":
                    det_devices = dict_item["detectors"]
                    det_list = re.findall(r'[^,;\s]+', det_devices)
                    start = float(dict_item["start"])
                    stop = float(dict_item["stop"])
                    delay = float(dict_item["delay"])
                    vel = float(dict_item["vel"])
                    md_str = dict_item["md"]
                    flyers = dict_item["flyer"]

                    md = {}
                    if md_str in ["None", " ", "NONE"]:
                        pass
                    else:
                        md_dict = ast.literal_eval(md_str)  # convert string into dict
                        md.update(md_dict)

                    item = {'item_type': 'plan',
                            'name': 'flyscan',
                            'args': [],
                            'kwargs': {
                                'detectors': det_list,
                                'flyer': flyers,
                                'start': start,
                                'stop': stop,
                                'delay': delay,
                                'vel': vel,
                                'md': md}
                            }
                    return item
        elif command == "xas":
            dict_item = self.checkInput(self.getValueList())
            end_station = dict_item["End_station"]
            mono = dict_item["Mono"]
            mode = dict_item["Mode"]
            vel = float(dict_item["Velocity(eV/s)"])
            start = float(dict_item["Start_Energy"])
            stop = float(dict_item["Stop_Energy"])
            step_size = float(dict_item["Step_Size(eV)"])
            current_range = dict_item["Current_Range"]
            use_shutter = bool(dict_item["Use_shutter"])

            md_str = dict_item["md"]
            md = {}
            if md_str in ["None", " ", "NONE"]:
                pass
            else:
                md_dict = ast.literal_eval(md_str)  # convert string into dict
                md.update(md_dict)
            item = {'item_type': 'plan',
                    'name': 'xas',
                    'args': [],
                    'kwargs': {'md': md, 'end_station': end_station, 'mono': mono, 'start_en': start, 'stop_en': stop,
                               'mode': mode, 'vel': vel, 'step_size': step_size
                              }
                   }
            print(f"The xas dict sent to queue server is {item}")
            return item
        elif command == " ":
            self.lineEdit.setText("please choose a plan...")
        else:
            self.lineEdit.setText("input is not valid")

    def createTable(self):
        """
        Create table (with plan parameters and default values)
        """
        _default_parameters_dict = self._get_default_parameters()
        self.param_dict = _default_parameters_dict.get(self.current_text)
        self.param_list = []
        self.insert_motornumber = 0
        for key in self.param_dict.keys():
            self.param_list.append(key)
        self._tableWidget.setRowCount(len(self.param_list))
        self._tableWidget.setColumnCount(2)
        self._tableWidget.horizontalHeader().setStretchLastSection(True)
        self._tableWidget.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self._tableWidget.setHorizontalHeaderLabels(('Parameters', 'Value'))
        if self.current_text != "xas":
            self._tableWidget.clear()
            self._tableWidget.setRowCount(len(self.param_list))
            self._tableWidget.setColumnCount(2)
            self._tableWidget.horizontalHeader().setStretchLastSection(True)
            self._tableWidget.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
            self._tableWidget.setHorizontalHeaderLabels(('Parameters', 'Value'))
            for i in range(len(self.param_list)):
                parameter = self.param_list[i]
                self._tableWidget.setItem(i, 0, QTableWidgetItem(parameter))
                self._tableWidget.setItem(i, 1, QTableWidgetItem(str(self.param_dict.get(parameter))))

        elif self.current_text == "xas":
            print("Choose xas from custom plan.")
            for i in range(len(self.param_list)):
                parameter = self.param_list[i]
                self._tableWidget.setItem(i, 0, QTableWidgetItem(parameter))
                if i not in [0, 1, 2, 3, 7, 8]:
                    self._tableWidget.setItem(i, 1, QTableWidgetItem(str(self.param_dict.get(parameter))))
            self.cb_end_station = QComboBox()
            self.cb_mono = QComboBox()
            self.cb_mode = QComboBox()
            self.cb_velocity = QComboBox()
            self.cb_current_range = QComboBox()
            self.cb_use_shutter = QComboBox()

            self._tableWidget.setCellWidget(0, 1, self.cb_end_station)
            self._tableWidget.setCellWidget(1, 1, self.cb_mono)
            self._tableWidget.setCellWidget(2, 1, self.cb_mode)
            self._tableWidget.setCellWidget(3, 1, self.cb_velocity)
            self._tableWidget.setCellWidget(7, 1, self.cb_current_range)
            self._tableWidget.setCellWidget(8, 1, self.cb_use_shutter)

            self.cb_end_station.addItems(self.param_dict.get("End_station"))
            self.cb_end_station.currentIndexChanged.connect(self.selectionchange)

    def selectionchange(self):
        """
        If an empty string is chosen in the end station, the subsequent options cannot be selected.
        Only when the first option of end station is selected as "OAESE" or "SISSY1", other options can be selected.
        """
        if self.cb_end_station.currentText() == " ":
            self.cb_mono.clear()
            self.cb_mode.clear()
            self.cb_velocity.clear()
            self.cb_current_range.clear()
            self.cb_use_shutter.clear()
        else:
            self.cb_mono.clear()
            mono_list = self.param_dict.get("Mono")
            self.cb_mono.addItem(mono_list[0])
            self.cb_mono.addItem(mono_list[1])
            self.cb_mono.addItem(mono_list[2])

            self.cb_mode.clear()
            self.cb_velocity.clear()
            self.cb_current_range.clear()
            self.cb_use_shutter.clear()
            self.cb_mode.addItems(self.param_dict.get("Mode"))
            self.cb_velocity.addItems(self.param_dict.get("Velocity(eV/s)"))
            self.cb_current_range.addItems(self.param_dict.get("Current_Range"))
            self.cb_use_shutter.addItems(self.param_dict.get("Use_shutter"))

        delegate = ReadOnlyDelegate(self)
        self._tableWidget.setItemDelegateForColumn(0, delegate)
        self._pb_cancel.setEnabled(True)
        self._pb_add_to_queue.setEnabled(True)

    def getValueList(self):
        """
        Get the user input of the plan from the table, all input must be filled. The default "None" value can be
        still "None".
        """
        dict_item = {}
        if self.current_text != "xas":
            parameters_number = self._tableWidget.rowCount()
            for i in range(0, parameters_number):
                dict_item[self._tableWidget.item(i, 0).text()] = self._tableWidget.item(i, 1).text()
        else:
            parameters_number = self._tableWidget.rowCount()
            for i in range(0, parameters_number):
                if i in [0, 1, 2, 3, 7, 8]:
                    dict_item[self._tableWidget.item(i, 0).text()] = self._tableWidget.cellWidget(i, 1).currentText()
                elif i in [4, 5, 6]:
                    input_result = self._tableWidget.item(i, 1).text()
                    if self.is_num(input_result):
                        if float(self._tableWidget.item(4, 1).text()) == float(self._tableWidget.item(5, 1).text()):
                            self.lineEdit.setText("Start Energy cannot be equal to stop energy.")
                        else:
                            dict_item[self._tableWidget.item(i, 0).text()] = self._tableWidget.item(i, 1).text()
                    else:
                        self.lineEdit.setText("Input values are not valid")
                else:
                    dict_item[self._tableWidget.item(i, 0).text()] = self._tableWidget.item(i, 1).text()
        return dict_item

    def is_num(self, str):
        """
        Check if the string can be converted into numbers.
        """
        try:
            return float(str)
        except ValueError:
            return False

    def klick_addMotors(self):
        """
        Add widgets of more motors for plan "scan", "mv", "mvr".
        """
        if self.current_text in ["scan"]:
            self.addscanMotors()
        elif self.current_text in ["mv", "mvr"]:
            self.addmvMotors()
        else:
            self.addMotor_button.setEnabled(False)

    def addmvMotors(self):
        """
        Add more motors and set_points in mv plan.
        """
        add_parameter_list = ["new mot " + str(self.insert_motornumber),
                              "new set_point " + str(self.insert_motornumber)]
        rowCount = self._tableWidget.rowCount()
        for i in range(len(add_parameter_list)):
            self._tableWidget.insertRow(rowCount)
            self.param_list.insert(rowCount + i + 1, add_parameter_list[i])
        self.insert_motornumber += 1
        for i in range(len(self.param_list)):
            parameter = self.param_list[i]
            self._tableWidget.setItem(i, 0, QTableWidgetItem(parameter))

    def addscanMotors(self):
        """
        Add more motors, start positions, stop positions in the scan plan.
        """
        add_parameter_list = ["new mot " + str(self.insert_motornumber), "new start " + str(self.insert_motornumber),
                              "new stop " + str(self.insert_motornumber)]
        rowCount = self._tableWidget.rowCount()
        for i in range(len(add_parameter_list)):
            self._tableWidget.insertRow(rowCount)
            self.param_list.insert(rowCount + i + 1, add_parameter_list[i])

        self.insert_motornumber += 1
        for i in range(len(self.param_list)):
            parameter = self.param_list[i]
            self._tableWidget.setItem(i, 0, QTableWidgetItem(parameter))


class QtRePlanEditor(QWidget):
    signal_update_widgets = Signal()
    signal_running_item_changed = Signal(object, object)

    def __init__(self, model, parent=None):
        super().__init__(parent)
        self.model = model.run_engine
        self._plan_viewer = _QtReViewer(self.model)
        self._plan_editor = _QtReEditor(self.model)
        self._emil_plans = EmilSpecialPlansWidget(self.model)
        self._tab_widget = QTabWidget()
        self._tab_widget.addTab(self._plan_viewer, "Plan Viewer")
        self._tab_widget.addTab(self._plan_editor, "Plan Editor")
        self._tab_widget.addTab(self._emil_plans, "Emil Plans")

        vbox = QVBoxLayout()
        vbox.addWidget(self._tab_widget)
        self.setLayout(vbox)

        self._plan_viewer.signal_edit_queue_item.connect(self.edit_queue_item)
        self._plan_editor.signal_switch_tab.connect(self._switch_tab)

    @Slot(str)
    def _switch_tab(self, tab):
        tabs = {"view": self._plan_viewer, "edit": self._plan_editor}
        self._tab_widget.setCurrentWidget(tabs[tab])

    @Slot(object)
    def edit_queue_item(self, queue_item):
        self._switch_tab("edit")
        self._plan_editor.edit_queue_item(queue_item)

