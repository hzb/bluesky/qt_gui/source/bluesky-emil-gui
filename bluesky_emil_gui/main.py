import argparse

from bluesky_widgets.qt import gui_qt

from .viewer import Viewer
from .settings import SETTINGS


def main(argv=None):
    print(__doc__)

    parser = argparse.ArgumentParser(description="bluesky-emil-gui")
    parser.add_argument("--zmq", help="0MQ address")
    parser.add_argument("--title",nargs='?', const="Bluesky Control", type=str, default ="Bluesky Control" )
    args = parser.parse_args(argv)

    with gui_qt(args.title):
        
        # Optional: Receive live streaming data.
        if args.zmq:
            SETTINGS.subscribe_to.append(args.zmq)
        viewer = Viewer(args)  # noqa: 401
    
    try:
        viewer.icat.logout()
        viewer.documents.close()
    except AttributeError as e:
        print(e)
    except NameError as e:
        print(e)


if __name__ == "__main__":
    main()