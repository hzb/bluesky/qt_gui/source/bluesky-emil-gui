import os

from qtpy.QtWidgets import (
    QTabWidget,
    QStatusBar,
    QLabel,
)
from bluesky_live.event import Event

from bluesky_live.event import CallbackException

from bluesky_widgets.models.auto_plot_builders import AutoLines, AutoImages
from bluesky_widgets.models.run_engine_client import RunEngineClient
from bluesky_widgets.qt import Window

from bluesky_widgets.models.plot_specs import Axes, Figure
from bluesky_widgets.models.plot_builders import Lines
from bluesky_widgets.models.auto_plot_builders import AutoPlotter
from bluesky_widgets.models.auto_plot_builders import AutoLines

from bessyii_bluesky_widgets.models.view_models import (
    SearchWithButton,
)

from bessyii_bluesky_widgets.qt.status_bar import QtStatusBar

from bessyii_bluesky_widgets.qt.zmq_table import QtZMQTableTab, LiveTableModel

from bessyii_bluesky_widgets.qt.QtConsole import IPythonConsoleTab

from bessyii_bluesky_widgets.models.elog_model import ElogModel

from bessyii_bluesky_widgets.models.icat_model import ICatModel

from bessyii_bluesky_widgets.models.document_model import DocumentModel

from bluesky_widgets.qt.run_engine_client import QtReConsoleMonitor

from bessyii_bluesky_widgets.models.view_models import (
    RunAndView, 
    SearchAndView,
)

from .settings import SETTINGS
from .pages.run_experiment import QtRunExperiment
from .pages.databroker import QtSearchAndView

class BessyiiRunEngineClient(RunEngineClient):
    def __init__(self, *args, icat, **kwargs):
        super().__init__(*args, **kwargs)
        self.icat = icat
        self._md_dict = {}
        self.editing=False

    def add_to_md(self, name):
        if not name in self._md_dict.keys():
            self._md_dict[name] = ''
            return True
        else:
            return False

    def remove_from_md(self, name):
        self.remove_key(self._md_dict, name)
        self._update_queue()

    def update_md(self, name, data):
        self._md_dict[name] = data
        self._update_queue()

    def remove_key(self, dict, key):
        try:
            del dict[key]
        except KeyError as e:
            pass
        except TypeError as e:
            pass

    def append_md(self, item):
        if not 'md' in item['kwargs']:
            return item
    
        if not self._md_dict:
            self.remove_key(item['kwargs']['md'], 'custom')
        else:
            item['kwargs']['md'].update({'custom': self._md_dict})

        if self.icat.md_dict == {}:
            self.remove_key(item['kwargs']['md'], 'icat')
            self.remove_key(item['kwargs']['md'], 'investigationid')
            self.remove_key(item['kwargs']['md'], 'name_of_investigation')
            self.remove_key(item['kwargs']['md'], 'title_of_investigation')
            self.remove_key(item['kwargs']['md'], 'user_name')
            self.remove_key(item['kwargs']['md'], 'user_profile')
        else:
            item['kwargs']['md'].update(self.icat.md_dict)
        
        return item

    def queue_item_add(self, *, item, params=None):
        super().queue_item_add(
            item=self.append_md(item),
            params=params,
        )
    
    def queue_item_update(self, *, item):
        super().queue_item_update(
            item=self.append_md(item),
        )

    def queue_item_add_batch(self, *, items, params=None):
        super().queue_item_add_batch(
            items=[self.append_md(item) for item in items],
            params=params,
        )
    
    def _update_queue(self):
        for item in self._plan_queue_items:
            try:
                self.queue_item_update(item=item)
            except CallbackException as e:
                pass

class ViewerModel:
    """
    This encapsulates on the models in the application.
    """

    def __init__(self, arguments):
        self.icat = ICatModel()
        self.elog = ElogModel()
        if arguments.zmq:
            self.documents = DocumentModel(ip=arguments.zmq)
        else:
            self.documents = DocumentModel(ip='localhost:5578')

        self.search = SearchWithButton(SETTINGS.catalog, columns=SETTINGS.columns)
        #self.auto_plot_builders = [AutoLines(max_runs=3)]
        #self.live_auto_plot_builders = [AutoLines(max_runs=3)]
 
        self.run_engine = BessyiiRunEngineClient(icat=self.icat)

        self.run_engine.events.add(queue_item_edit=Event)

        self.search.events.view.connect(self._on_view)

        self.icat.events.investigation_changed.connect(self._on_investigation_changed)

    def _on_view(self, event):
        catalog = self.search.selection_as_catalog
        if catalog is None:
            return
        for uid, run in catalog.items():
            for auto_plot_builder in self.auto_plot_builders:
                try:
                    auto_plot_builder.add_run(run, pinned=True)
                except TypeError:
                    auto_plot_builder.add_run(run)
    
    def _on_investigation_changed(self, event):
        self.run_engine._update_queue()
        

class Viewer(ViewerModel):
    """
    This extends the model by attaching a Qt Window as its view.

    This object is meant to be exposed to the user in an interactive console.
    """

    def __init__(self, arguments,*, show=True):
        super().__init__(arguments)
        widget = QtViewer(self)
        self.arguments = arguments
        self._window = Window(widget, show=show)
        self.status_bar = QtStatusBar(widget, self)
        self._window._qt_window.setStatusBar(self.status_bar)


    @property
    def window(self):
        return self._window

    def show(self):
        """Resize, show, and raise the window."""
        self._window.show()

    def close(self):
        """Close the window."""
        self._window.close()

class QtViewer(QTabWidget):
    def __init__(self, model, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.model = model
        self.setTabPosition(QTabWidget.West)

        self._re_manager = QtRunExperiment(self.model)
        self.addTab(self._re_manager, "Run Engine")

        #self._search_and_view = QtSearchAndView(self.model)
        #self.addTab(self._search_and_view, "Data Broker")

        self._live_table = QtZMQTableTab(self.model)
        self.addTab(self._live_table, "Live Table")

        self._qt_console = IPythonConsoleTab(self.model)
        self.addTab(self._qt_console, "IPython Console")
    
    def closeEvent(self, *args, **kwargs):
        print('CLOOOSE')