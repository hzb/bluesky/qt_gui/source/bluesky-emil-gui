#!/bin/bash

export QSERVER_ZMQ_ADDRESS=tcp://172.17.10.16:60615
# activate bluesky virtual environment
. /home/emilbeam/bluesky/bluesky_venv/bin/activate

#Start the gui
bluesky-emil-gui --catalog sissy2 --zmq 172.17.10.16:5578
