# Minimal GUI

This pyQT based GUI aims to:

1. Provide an interface to any bluesky queueserver and using the widgets in bessyii_bluesky_widgets allow users to create forms for editing plans without needed to change any of the GUI code, only using plan annotation
2. Allow a user to modify plans in the queue
3. Allow a user to see the status of plans in the queue
4. Allow a user to see old plans in the queue history
5. See the status of a currently running plan
6. Use a live table to observe a currently running plan
7. Interact with an IPython kernel that is exposed by the queueserver. 

The GUI includes no live plotting and no ability to interact with data. This design is more reliable for now. 

## Installation

For an example deployment using a container see: https://codebase.helmholtz.cloud/hzb/bluesky/qt_gui/images/minimal-qt-gui-image

You can set the following environment variables

| Env Var  |      Description   | Example |
|----------|-------------|------|
| BATCH_SAVE_DIR |  The directory that batches are saved to and loaded from | "/opt/batches" |
| QSERVER_ZMQ_CONTROL_ADDRESS| The address used by the Queserver for Control |   "tcp://localhost:60616"  |
| QSERVER_ZMQ_INFO_ADDRESS| The address used by the Queserver for monitoring | "tcp://localhost:60626"|

You can set the title of the GUI using the command line argument `--title`

To start the gui type `bluesky_emil_gui`

## To-Do

Remove mention of EMIL